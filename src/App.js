import React, { Component } from 'react';
import './App.css';
import Rest from './components/Rest';
import Welcome from './components/WelcomeFunc';
import WelcomeC from './components/WelcomeClass';
import Message from './components/Message';
import Counter from './components/Counter';
import EventBind from './components/EventBind';
import ParentComponent from './components/ParentComponent';
import UserGreeting from './components/UserGreeting';

class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <Welcome name = 'Function User'>
            <Rest />
            <p>Rest Api</p>
        </Welcome> 
        <Message /> */}
        {/* <WelcomeC name = 'Class User'>
          <Rest />
        </WelcomeC> */}
        {/* <Counter incValue = {1} /> */}
        {/* <EventBind /> */}
        {/* <ParentComponent /> */}
        <UserGreeting />
      </div>
    );
  }
}

export default App;
