import React, { Component } from 'react'

class Counter extends Component {
  constructor(props) {
      super(props)

      this.state = {
          count : 0
      }
  }

  Increment() {
    // this will not consider previous state
    //   this.setState(
    //       { count: this.state.count + 1 },
    //       () => {console.log('Sync : ', this.state.count)} //Synchronous callback func
    //   )
    //   console.log('Async : ', this.state.count) // Asynchronous

    this.setState((prevState, props) => ({ 
        count : prevState.count + props.incValue
    }),
    () => {console.log('Sync : ', this.state.count)} //Synchronous callback func
    )
    console.log('Async : ', this.state.count) // Asynchronous
  }

  render() {
    return (
      <div>
        <h3>Count : {this.state.count}</h3>
        <button onClick={() => this.Increment()}>Count</button>
      </div>
    )
  }
}

export default Counter
