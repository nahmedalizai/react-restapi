import React from 'react';

const Rest = () => {
    // JSX
    // return  (
    //     <div>
    //         <h1>Rest Api</h1>
    //     </div>
    // )

    //React CreateElement
    return React.createElement(
        'div', 
        null, // this defines the properties of the element
        React.createElement(
                'p',
                { id : 'hOne', className : 'styleclass'}, 
                'Rest API'
            )
    )
}

export default Rest