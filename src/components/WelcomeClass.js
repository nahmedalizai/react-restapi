import React, {Component}  from 'react';

class WelcomeC extends Component {
    render() {
        const {name, children} = this.props
        return (
            <div>
                <h1>Welcome {name}</h1>
                {children}
            </div>
        )
    }
}

export default WelcomeC